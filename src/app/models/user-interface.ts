export interface UserInterface {
  id?: string;
  name?: string;
  emailAddress?: string;
  password?: string;
}
